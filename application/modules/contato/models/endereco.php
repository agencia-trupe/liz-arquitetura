<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Endereço
*
* @author Nilton de Freitas
* @link http://trupe.net
*
*/
Class Endereco extends Datamapper{
    var $table = 'contatos';

     public function __construct()
    {
        // model constructor
        parent::__construct();
    }

    function get_info()
    {
        $endereco = new Endereco();
        $endereco->where('id', '1');
        $endereco->limit(1);
        $endereco->get();

        return $endereco;
    }

    function atualiza($dados)
    {
        $endereco = new Endereco();
        $endereco->where('id', 1)->get();
        $update = $endereco->update(array(
                'telefone' => $dados['telefone'],
                'email' =>  $dados['email'],
                'ddd' =>  $dados['ddd'],
                'endereco' =>  $dados['endereco'],
                'cidade' =>  $dados['cidade'],
                'cep' =>  $dados['cep'],
                'uf' =>  $dados['uf'],
                'facebook' =>  $dados['facebook'],
                'twitter' =>  $dados['twitter'],
                'instagram' =>  $dados['instagram'],
            ));
        return $update;
    }

    function change($dados)
    {
        $contato = new Endereco();
        $contato->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update = $contato->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

}