<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Contato
* 
* @author Nilton de Freitas - Trupe 
* @link http://trupe.net
*
*/
Class Contato extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contato/endereco');
    }
    var $data;
    /**
     * Exibe o formulário de contato
     */
    public function index()
    {
        $seo = array(
            'title' => 'Contato',
            'description' => 'Entre em contato com a Lidiane Lourenço Arquitetura'
        );
        $this->load->library('seo', $seo);
        $this->data = array(
            'titulo' => 'Contato',
            'page_title' => 'Contato',
            'page_description' => 'Uma maneira rápida de solicitar informações ou visita na sua empresa',
            'pagina' => 'contato',
            'conteudo' => 'contato/index'
        );
        $this->data['contato'] = $this->endereco->get_info();
        $this->load->view('layout/template', $this->data);
    }

    function endereco($tipo = NULL)
    {
        $this->load->model('endereco');
        $data['endereco'] = $this->endereco->get_endereco();
        if($tipo == 'header')
        {
            $this->load->view('contato/endereco_header', $data);
        }
        elseif($tipo == 'contato')
        {
            $this->load->view('contato/endereco_contato', $data);
        }
        elseif($tipo == 'mail')
        {
            $this->load->view('contato/endereco_mail', $data);
        }
    }

    function parcial()
    {
        $this->load->model('contato/endereco');
        $this->data['contato'] = $this->endereco->get_info();
        $this->load->view('contato/parcial', $this->data);
    }
    
    function sociais()
    {
         $this->data['contato'] = $this->endereco->get_info();
         $this->load->view('contato/sociais', $this->data);
    }

    function ajax_check() 
    {
        $this->load->library('form_validation');
        $this->load->library('typography');
        $this->form_validation->set_error_delimiters('','');

        if($this->input->post('ajax') == '1') 
        {
            if($this->form_validation->run('contato') == FALSE) 
            {
                    echo validation_errors();
            } 
            else 
            {
                $this->load->library('MY_PHPMailer');
                $mail = new PHPMailer();
                // Define os dados do servidor e tipo de conexão
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                
                $mail->isSMTP();
                $mail->Host = 'br296.hostgator.com.br';
                $mail->SMTPAuth = true;
                $mail->Username = 'noreply@lizarquitetura.com.br';
                $mail->Password = 'senhatrupe12';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;
                // Define o remetente
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->From = "contato@lizarquitetura.com.br"; // Seu e-mail
                $mail->FromName = $this->input->post('nome'); // Seu nome
                $mail->AddReplyTo($this->input->post('email'),$this->input->post('nome'));
                 
                // Define os destinatário(s)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->AddAddress('contato@lizarquitetura.com.br', 'Site Liz Arquitetura');
                //$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
                $mail->AddBCC('luiz@trupe.net', 'Trupe'); // Cópia Oculta
                 
                // Define os dados técnicos da Mensagem
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
                $mail->WordWrap = 50;
                //$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)
                 
                // Define a mensagem (Texto e Assunto)
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $mail->Subject  = '=?UTF-8?B?'.base64_encode("Contato do Site Liz Arquitetura").'?='; // Assunto da mensagem
                
                $data['dados'] = array(
                                    'nome' => $this->input->post('nome'),
                                    'email' => $this->input->post('email'),
                                    'telefone' => $this->input->post('telefone'),
                                    'assunto' => $this->input->post('assunto'),
                                    'mensagem' => $this->typography->auto_typography($this->input->post('mensagem')),
                            );
                $email_view = $this->load->view('contato/email', $data, TRUE);

                $mail->Body = $email_view;


                if(!$mail->Send()) {
                  echo "Erro: " . $mail->ErrorInfo;
                } else {
                  echo "Mensagem enviada com sucesso!";
                }
            }
        }
    }
}

