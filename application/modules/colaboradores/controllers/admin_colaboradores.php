<?php
class Admin_colaboradores extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'colaboradores';
        $this->load->model('colaboradores/colaborador');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['colaboradores'] = $this->colaborador->get_all();
            $this->data['conteudo'] = 'colaboradores/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->load->model('colaboradores/colaborador');
            $this->data['colaborador'] = $this->colaborador->get_conteudo($id, 'id');
            $this->data['acao'] = 'editar';
            $this->data['conteudo'] = 'colaboradores/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'colaboradores/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('colaborador'))
            {
                $this->data['colaborador'] = $this->colaborador->get_conteudo($this->input->post( 'id' ), 'id');
                $this->data['conteudo'] = 'colaboradores/admin_edita';
                $this->data['acao'] = 'editar';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {

                    $config['upload_path'] = './assets/img/colaboradores/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '4000';
                    $config['max_width']  = '4000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( !$this->upload->do_upload('imagem'))
                    {
                            $data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['colaborador'] = $this->colaborador->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'colaboradores/admin_edita';
                            $this->data['acao'] = 'editar';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize(600,600,TRUE)
                                 ->save($new_file,true)
                            &&
                            $this->image_moo
                                 ->load($new_file)
                                 ->resize_crop(100,170)
                                 ->save($thumb,true)
                            )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['imagem'] = NULL;
                        }
                    }
                }
                $post['updated'] = time();
                if($this->colaborador->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/colaboradores');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/colaboradores/edita/' . $post['id']);
                }
            }
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('colaborador'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'colaboradores/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {

                    $config['upload_path'] = './assets/img/colaboradores/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '4000';
                    $config['max_width']  = '1600';
                    $config['max_height']  = '1200';

                    $this->load->library('upload', $config);

                    if ( !$this->upload->do_upload('imagem'))
                    {
                            $this->data['acao'] = 'cadastrar';
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['colaborador'] = $this->colaborador->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'colaboradores/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(600,600)
                                 ->save($new_file,true)
                            &&
                            $this->image_moo
                                 ->load($new_file)
                                 ->resize_crop(100,170)
                                 ->save($thumb,true)
                            )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['imagem'] = NULL;
                        }
                    }
                }
                $post['updated'] = time();
                if(is_null($post['imagem']))
                {
                    $this->session->set_flashdata('error', 'Você precisa selecionar uma imagem.');
                    redirect('painel/colaboradores/cadastrar');
                }
                elseif($this->colaborador->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/colaboradores');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/colaboradores/cadastrar');
                }
            }
        }
    }

    public function apagar($id)
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->colaborador->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/colaboradores');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/colaboradores');
            }
        }
    }

    public function adiciona_foto()
    {
        $this->load->model('colaboradores/clipping');

        $this->load->library('form_validation');

        $status = "";
        $msg = "";
        $file_element_name = 'projeto-foto-upload';

        $config['upload_path'] = './assets/img/colaborador/fotos';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']  = 1024 * 8;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $id = '';
            $nome = '';
        }
        else
        {
            $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
            $upload_data = $this->upload->data();
            $file_uploaded = $upload_data['full_path'];
            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
            $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];

            if(
                $this->image_moo->load($file_uploaded)
                                ->resize_crop(478,560)
                                ->save($new_file,true)
                &&
                $this->image_moo->load($new_file)
                                ->resize_crop(242,242)
                                ->save($thumb, true)
              )
            {
                $imagem = $upload_data['file_name'];
                    $data = array(
                    'imagem' => $imagem,
                    'midia_id' => $this->input->post('midia_id'),
                    'titulo' => 'Teste'
                    );
                $adiciona = $this->clipping->insert($data);
                if($adiciona)
                {
                    $status = "success";
                    $msg = "Foto adicionada com sucesso.";
                    $id = $adiciona;
                    $nome = $data['imagem'];
                }
                else
                {
                    unlink($data['file_data']['full_path']);
                    $status = "error";
                    $msg = "Houve um erro ao processar sua requisição. Por vavor, tente novamente, ou entre em contato com o suporte.";
                    $id = '';
                    $nome = '';
                }
            }
            else
            {
                $status = 'error';
                $msg = $this->image_moo->display_errors();
                $id = '';
                $nome = '';
            }
        }
        @unlink($_FILES[$file_element_name]);
        echo json_encode(array('status' => $status, 'msg' => $msg, 'id' => $id, 'nome' => $nome));
    }

    public function deleta_foto()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->load->model('colaboradores/clipping');

            if($this->input->post('ajax') == 1)
            $deleta = $this->clipping->deleta_foto($this->input->post('foto_id'));
            if($deleta)
            {
                $response['status'] = 'success';
                $response['msg'] = 'Foto apagada com sucesso.';
            }
            else
            {
                $response['status'] = 'error';
                $response['msg'] = 'Erro ao apagar, por favor,
                tente novamente';
            }
            echo json_encode($response);
        }
    }

    /**
     * Reordena os tipos de projetos para a exibição
     * @return void status do processamento
     */
    public function sort_colab()
    {
        if (!$this->tank_auth->is_logged_in())
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Lidiane Lourenço - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('colaborador');
            if ($itens)
            {
                $ordenar = $this->colaborador->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
            }
            else
            {
              echo 'Erro!';
            }
        }
    }
}