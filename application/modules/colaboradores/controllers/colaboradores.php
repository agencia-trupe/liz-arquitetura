<?php

class Colaboradores extends MX_Controller
{
	var $data;

	public function parcial()
	{
		$this->load->model('colaboradores/colaborador');
		$this->data['colaboradores'] = $this->colaborador->get_all();
		$this->data['conteudo'] = 'colaboradores/parcial';
		$this->load->view($this->data['conteudo'], $this->data);
	}
}