<div class="row-fluid">
    <div class="span9">
        <legend>Colaboradores
                <?php echo anchor('painel/colaboradores/cadastrar', 'Novo', 'class="btn btn-info btn-mini"'); ?>
                <a href="#" class="ordenar-colaboradores btn btn-mini btn-info">ordenar ítens</a>
                <a href="#" class="salvar-ordem-colaboradores hide btn btn-mini btn-warning">salvar ordem</a>
        </legend>
        <div class="alert alert-info hide colaboradores-mensagem">
            <span>Para ordenar, clique na miniatura do colaborador e arraste até a posição desejada</span>
            <a class="close" data-dismiss="alert" href="#">&times;</a>
        </div>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span2">Foto</th><th>Nome</th><th class="span2"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php if($colaboradores): ?>
            <?php foreach ($colaboradores as $colaborador): ?>
                <tr id="colaborador_<?php echo $colaborador->id ?>">
                    <td><img src="<?php echo base_url('assets/img/colaboradores/thumbs/' . $colaborador->imagem) ?>" alt="<?php echo $colaborador->nome ?>"></td>
                    <td><?=$colaborador->nome; ?></td>
                    <td><?=anchor('painel/colaboradores/editar/' . $colaborador->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                    <?=anchor('painel/colaboradores/apagar/' . $colaborador->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>