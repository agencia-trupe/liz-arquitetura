<?php if ($colaboradores) { echo '<h2>Colaboradores</h2>'; } ?>
<?php foreach ($colaboradores as $colaborador): ?>
	<div class="colaborador">
		<div class="colaborador-imagem">
			<img src="<?php echo base_url('assets/img/colaboradores/thumbs/' . $colaborador->imagem) ?>" alt="<?php echo $colaborador->nome ?>">
		</div>
		<div class="colaborador-texto">
			<h3><?php echo $colaborador->nome ?></h3>
			<?php echo $colaborador->texto ?>
		</div>
		<div class="clearfix"></div>
	</div>
<?php endforeach ?>