<div class="conteudo-projetos projetos-paisagismo projetos-detalhe">
	<div class="interna">
		<h1>Igaratá</h1>
		<div class="projetos-detalhe-wrapper">
			<div id="projetos-detalhe-tabs">
			  	<div class="projetos-detalhe-ampliada">
			  		<div id="foto1">
			  			<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/ampliada-1.jpg') ?>" alt="">
				  	</div>
				  	<div id="foto2">
				  		<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/ampliada-2.jpg') ?>" alt="">
				  	</div>
				  	<div id="foto3">
				  		<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/ampliada-3.jpg') ?>" alt="">
				  	</div>
					<div id="foto4">
						<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/ampliada-4.jpg') ?>" alt="">
				  	</div>
			  	</div>
			  	<ul class="projetos-detalhe-miniaturas">
			    	<li>
			    		<a class="bwWrapper" href="#foto1">
			    			<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/thumb-1.jpg') ?>" alt="">	
			    		</a>
			    	</li>
			    	<li>
			    		<a class="bwWrapper" href="#foto2">
			    			<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/thumb-2.jpg') ?>" alt="">
			    		</a>
			    	</li>
			    	<li>
			    		<a class="bwWrapper" href="#foto3">
							<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/thumb-3.jpg') ?>" alt="">
			    		</a>
			    	</li>
			    	<li>
			    		<a class="bwWrapper" href="#foto4">
							<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/thumb-4.jpg') ?>" alt="">
			    		</a>
			    	</li>
			  	</ul>
			  	<div class="clearfix"></div>
			</div>
			<a href="<?php echo site_url('projetos/paisagismo') ?>" class="projetos-detalhe-voltar">
				voltar
			</a>
			<div class="projetos-detalhe-social">
				<a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
				<div class="g-plusone" data-size="medium"></div>
				<script src="//platform.linkedin.com/in.js" type="text/javascript">
				 lang: en_US
				</script>
				<script type="IN/Share" data-counter="right"></script>
				<iframe class="facebook-share" src="//www.facebook.com/plugins/like.php?href=<?php echo current_url(); ?>&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=355115464591773" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px;" allowTransparency="true"></iframe>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="separador-pagina"></div>
	<div class="interna">
		<div class="outros-projetos">
			<h2>Veja outros projetos</h1>
			<div class="outros-projetos-wrapper">
				<a href="<?php echo site_url('projetos/paisagismo/jardins') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/2_jardins/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/casa-pascoal-leite') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/3_casa_pascoal_leite/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/panamby') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/1_panamby/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/campo-belo') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/5_campo_belo/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/paisagismo/brooklin') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/paisagismo/6_brooklin/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/interiores/pascoal-leite') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/interiores/1_pascoal_leite/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
				<a href="<?php echo site_url('projetos/interiores/brooklin') ?>" class="outro-projeto">
					<img src="<?php echo base_url('assets/img/projetos/interiores/2_brooklin/outros-projetos.jpg') ?>" alt="">
					<span class="hover"></span>
				</a>
				<div class="outro-projeto">
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>