<div class="conteudo-perfil">
	<div class="interna">
		<div class="perfil-texto">
			<?php echo $dados_pagina->texto ?>
		</div>
		<div class="perfil-imagem">
			<img src="<?php echo base_url('assets/img/paginas/' . $dados_pagina->imagem ) ?>" alt="Lidiane Lourenço">
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="separador-pagina"></div>
	<div class="interna colaboradores">
			<?php echo Modules::run('colaboradores/parcial') ?>
	</div>
</div>
<div class="clearfix"></div>