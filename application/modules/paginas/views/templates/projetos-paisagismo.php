<div class="conteudo-projetos projetos-paisagismo">
	<div class="interna">
		<div class="projetos-lista-slider-wrapper">
			<div class="projetos-lista-slider">
				<div role="projeto-slide">
					<div class="projetos-lista-thumbs-wrapper">
						<a href="<?php echo site_url('projetos/paisagismo/brooklin') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/paisagismo/6_brooklin/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Brooklin</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/paisagismo/jardins') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/paisagismo/2_jardins/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Jardins</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/paisagismo/casa-pascoal-leite') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/paisagismo/3_casa_pascoal_leite/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Casa Pascoal Leite</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/paisagismo/igarata') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/paisagismo/4_igarat/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Igaratá</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/paisagismo/campo-belo') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/paisagismo/5_campo_belo/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Campo Belo</span>
							</span>
						</a>
					</div>
				</div>
				<div role="projeto-slide">
					<div class="projetos-lista-thumbs-wrapper">
						<a href="<?php echo site_url('projetos/paisagismo/panamby') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/paisagismo/1_panamby/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Panamby</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<div class="clearfix"></div>