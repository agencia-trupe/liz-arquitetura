<div class="row-fluid">
    <div class="span9">
        <legend>Serviços <a class="btn btn-info btn-mini" href="<?=site_url('painel/servicos/cadastrar'); ?>">Novo</a></legend>
     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span7">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($servicos as $servico): ?>
                <tr>
                    <td><?=$servico->titulo; ?></td>
                    <td><?=anchor('servicos/admin_servicos/editar/' . $servico->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('servicos/admin_servicos/deleta_servico/' . $servico->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>