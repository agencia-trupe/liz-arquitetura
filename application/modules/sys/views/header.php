<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <meta name="viewport" content="width=device-width,
                               maximum-scale=1.0" />
       <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
        <script src="<?php echo base_url(); ?>assets/painel/js/vendor/custom.modernizr.js"></script>

        <title><?=$this->seo->get_title(); ?></title>
    </head>
    <body>
        <div class="clearfix"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

<nav class="top-bar">
  <ul class="title-area">
    <!-- Title Area -->
    <li class="name">
      <h1><a href="#"><?=$this->seo->site_name(); ?></a></h1>
    </li>
    <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>

  <section class="top-bar-section">
    <!-- Left Nav Section -->
     <ul class="left">
      <li class="divider"></li>
      <li class="<?=($pagina == 'paginas') ? 'active' : ''; ?>"><a href="#">Main Item 1</a></li>
      <li class="divider"></li>
    </ul>
  </section>
</nav>