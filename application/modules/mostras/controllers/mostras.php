<?php
class Mostras extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mostras/mostra');
        $this->load->model('mostras/fotomostra');
        $this->data['pagina'] = 'mostras';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $this->data['mostras'] = $this->mostra->get_all();
        $this->data['conteudo'] = 'mostras/index';
        $seo = array(
            'titulo' => 'mostras',
            'descricao' =>  'Conceça os mostras realizados pela Lidiane Lourenço
                            arquitetura de interiores' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function detalhe( $id )
    {
        $this->load->model( 'mostras/fotomostra' );
        //busca os dados de uma página cujo slug foi passado como parâmetro
        $mostra = $this->mostra->get_conteudo( $id );
        if( !is_null( $mostra ) )
        {
            $this->data['fotomostras'] = $this->fotomostra->get_mostra($mostra->id);
            //Variável com os dados da página a ser enviada para a view
            $this->data['mostra'] = $mostra;
            //Variáveis com os ids de posts anterior e próximo.
            $this->data['prev'] = $this->mostra->get_related( $mostra->ordem, 'prev' );
            $this->data['next'] = $this->mostra->get_related( $mostra->ordem, 'next' );
            //Carrega a biblioteca de SEO e a inicializa.
            $seo = array(
                'title' => $mostra->titulo,
                'description' => $mostra->descricao,
                );
            $this->load->library( 'seo', $seo );
            //Define a view utilizada
            $this->data['conteudo'] = 'mostras/detalhe';
            //Carrega a view especificada como parâmetro e exibe a página
            $this->load->view( 'layout/template', $this->data );
        }
        else
        {
            show_404();
        }
    }

    private function _insert()
    {
        for ($i=0; $i < 9; $i++) { 
            $dados = array();
            $dados['titulo'] = 'mostra ' . $i;
            $dados['data'] = 2010;
            $dados['texto'] = 'Descrição do mostra ' . $i;
            $dados['capa'] = 'mostra_' . $i . '.jpg';
            $insert = $this->mostra->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }

    public function insert_fotomostra($mostra_id)
    {
        for ($i=0; $i < 30; $i++) { 
            $dados = array();
            $dados['titulo'] = 'Foto ' . $i;
            $dados['imagem'] = 'fotomostra_exemplo.jpg';
            $dados['mostra_id'] = $mostra_id;
            $insert = $this->fotomostra->insert($dados);
            if($insert)
            {
                echo 'Ok!';
            }
            else
            {
                echo 'Nooooo!';
            }
        }
    }
}