<div class="span9">

  <legend>Editar 
    <? switch ($campo) {
      case 'descricao':
        echo 'Descrição';
        break;
      case 'missao':
        echo 'Missão';
        break;
      case 'visao':
        echo 'Visão';
        break;
      case 'valores':
        echo 'Valores';
        break;
    }
    ?>
  </legend>
  <?=isset($error) ? $error['error'] : ''; ?>
  <?=form_open_multipart(($acao == 'editar') ? 'painel/empresa/atualiza' : 'painel/equipe/salva', 'class="well"'); 
  ?>
  <div class="row-fluid">
    <?=($acao == 'edita') ? form_hidden('acao', 'edita') : form_hidden('acao', 'cadastra'); ?>  
  </div>
  <?php if($acao == 'editar') : ?>
    <?=form_hidden('id', set_value('id', $empresa->id)); ?>
    <?=form_hidden('campo', set_value('campo', $campo)); ?>
  <?php endif; ?>
  <div class="row-fluid">
      <div class="control-group">
            <label class="control-label" for="texto">Texto</label>
            <div class="controls">
              <?=form_textarea('texto', set_value('texto', $acao == 'editar' ? $empresa->$campo : ''), 'class="tinymce span11"'); ?>
              <span class="help-inline"><?=form_error('texto'); ?></span>
            </div>
      </div>
     </div>
  <?=form_submit('submit', ($acao == 'editar') ? 'Salvar' : 'Cadastrar', 'class="btn btn-primary"'); ?>
  <?=form_close(); ?> 
</div>