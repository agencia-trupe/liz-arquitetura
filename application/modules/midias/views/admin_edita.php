<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
            <div class="alert alert-error">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        <?php endif; ?> 
        <?php if($this->session->flashdata('success') != NULL): ?>
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        <?php endif; ?>
        <?php if(isset($error)): ?>
            <div class="alert alert-error">
                <?php print_r($error); ?>
            </div>
        <?php endif; ?>
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Clipping</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'midias/admin_midias/processa';
                    break;
                
                default:
                    $action = 'midias/admin_midias/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
        <input type="hidden" name="id" value="<?php echo $midia->id ?>" class="id" >
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ($acao == 'editar') ? $midia->titulo : ''),
        'class' => 'span5'
    )); ?>
    <?=form_error('titulo'); ?>
    <!--<select>
        <?php //foreach( $templates as $template ): ?>
        <option value="<?//=str_replace('.php', '', $template); ?>">
            <?//=ucfirst(str_replace('.php', '', $template)); ?>
        </option>
        <?php //endforeach; ?>
    </select>-->
    <br>
    <div class="clearfix"></div>
    <br>

     <?php if($acao == 'editar' && $midia->imagem):?>
     <img src="<?php echo base_url(); ?>assets/img/clipping/thumbs/<?php echo $midia->imagem; ?>" alt="" >
     <?php endif; ?>
     <br>
     <div class="control-group">
            <label class="control-label" for="imagem"><?php echo ( $acao == 'editar' ) ? 'Alterar Imagem' : 'Adicionar Imagem'; ?></label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <!--<div class="row-fluid">
        <div class="alert alert-info span4">Criada em <?//=date( 'd/m/Y', $midia->created ); ?></div>
        <div class="alert alert-success span4 ">Última atualização: 
            <?//=(isset($midia->updated)) ? date( 'd/m/Y', $midia->updated ) : 'Nunca'; ?></div>
    </div>-->
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('midias/admin_midias/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    <?php if ( $acao == 'editar' ): ?>
    <legend>Fotos do Clipping
        <a href="#" class="btn btn-mini btn-info btn-adicionar-foto">
            <i class="icon-picture icon-white"></i> Adicionar Foto
        </a>
    </legend>
    <div class="row">
        <div class="form-adicionar-foto well span8 invisible">
            <?=form_open('','id="clipping-upload"'); ?>
            <?=form_upload('projeto-foto-upload', '', 'id="projeto-foto-upload"'); ?>
            <input type="submit" class="btn btn-mini btn-success btn-adicionar-foto-upload"
            value="Fazer upload" >  
            <a href="#" class="btn btn-mini btn-danger btn-adicionar-foto-cancela">
            <i class="icon-remove-sign icon-white"></i> Cancelar</a>
        </div>
    </div> 
    <div class="fotos-lista">
        <ul id="midia-images" class="ui-sortable" style="list-style-type:none; padding:0">
        <?php if (isset($clipping)): ?>
        <?php foreach ($clipping as $foto): ?>
            <li class="projeto-foto" id="foto_<?=$foto->id; ?>">
                <img width="121" height="121" style="margin-bottom:10px;" src="<?=base_url('assets/img/clipping/thumbs/' . $foto->imagem); ?>" alt="">
                <a href="#" data-module="midia" data-id="<?=$foto->id; ?>" class="btn btn-delete btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
            </li>
        <?php endforeach; ?>
        <?php endif; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <?php else: ?>
    <div class="alert alert-info">
        <span>Para adicionar mais fotos ao clipping, salve-o e utilize a opção <em>editar</em></span>
    </div>
    <?php endif; ?>
    </div>
</div>