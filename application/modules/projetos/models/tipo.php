<?php
class Tipo extends Datamapper
{
    var $table = 'tipos_projetos';

    function get_all()
    {
        $projeto = new Tipo();
        $projeto->order_by( 'ordem', 'ASC' )->get();
        $arr = array();
        foreach( $projeto->all as $projeto )
        {
            $arr[] = $projeto;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo( $id )
    {
        $projeto = new Tipo();
        $projeto->where( 'id', $id )->get();
        if( ! $projeto->exists() ) NULL;
        return $projeto;
    }

    function insert($dados)
    {
        $tipos = new Tipo();
        $tipos->get();

        $count = $tipos->result_count();

        $projeto = new Tipo();
        foreach ($dados as $key => $value)
        {
            $projeto->$key = $value;
        }
        $projeto->ordem = $count;
        $projeto->created = time();
        $insert = $projeto->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change($dados)
    {
        $projeto = new Tipo();
        $projeto->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update = $projeto->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $tipo = new Tipo();
        $tipo->where('id', $id)->get();
        if($tipo->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $categoria = new Tipo();
            $categoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($categoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}