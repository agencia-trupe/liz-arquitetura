<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Projeto</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'projetos/admin_projetos/processa';
                    break;
                
                default:
                    $action = 'projetos/admin_projetos/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$projeto->id; ?>" class="id" />
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar' ) ? $projeto->titulo : '')
    )); ?>
    <?=form_error('titulo'); ?>
    <?=form_label('Categoria'); ?>
    <select name="tipos_projetos_id">
        <?php foreach ($categorias as $categoria): ?>
            <option value="<?php echo $categoria->id ?>" <?php if($acao == 'editar'){echo ($projeto->tipos_projetos_id == $categoria->id) ? 'selected' : '';} ?>><?php echo $categoria->nome ?></option>
        <?php endforeach ?>
    </select>
    <br>
    <?php if ($acao == 'editar') : ?>
    <img src="<?php echo base_url(); ?>assets/img/projetos/thumbs/<?php echo $projeto->capa; ?>" alt="" >
    <?php endif; ?>
    <div class="control-group">
            <label class="control-label" for="imagem">Foto de capa</label>
            <div class="controls">
              <?php echo form_upload('imagem', set_value('imagem')); ?>
              <span class="help-inline"><?php echo form_error('imagem'); ?></span>
            </div>
     </div>
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('projetos/admin_projetos/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    <?php if ( $acao == 'editar' ): ?>
    <legend>Fotos do projeto 
        
        <a href="#" class="<?=($fotos == NULL) ? 'invisible' : ''?> btn btn-mini btn-info btn-reordenar">
            <i class="icon-retweet icon-white"></i> Reordenar
        </a>
        <a href="#" class="btn btn-mini btn-info btn-adicionar-foto">
            <i class="icon-picture icon-white"></i> Adicionar Foto
        </a>
    </legend>
    <div class="row">
        <div class="form-adicionar-foto well span4 invisible">
            <?=form_open('','id="projetos-upload"'); ?>
            <?=form_upload('projeto-foto-upload', '', 'id="projeto-foto-upload"'); ?>
            <input type="submit" class="btn btn-mini btn-success btn-adicionar-foto-upload"
            value="Fazer upload" >  
            <a href="#" class="btn btn-mini btn-danger btn-adicionar-foto-cancela">
            <i class="icon-remove-sign icon-white"></i> Cancelar</a>
        </div>
    </div> 
    <div class="fotos-lista">
        <ul id="projeto-images" class="ui-sortable" style="list-style-type:none; padding:0">
        <?php if($fotos != NULL): ?>
        <?php foreach ($fotos as $foto): ?>
            <li class="projeto-foto" id="foto_<?=$foto->id; ?>">
                <img width="121" height="121" style="margin-bottom:10px;" src="<?=base_url('assets/img/projetos/thumbs/' . $foto->imagem); ?>" alt="">
                <a href="#" data-id="<?=$foto->id; ?>" data-module="projetos" class="btn btn-delete btn-mini btn-danger"><i class="icon-trash icon-white"></i></a>
            </li>
        <?php endforeach; ?>
        <?php endif; ?>
        </ul>
        <div class="clearfix"></div>
    </div>
    <?php else: ?>
    <div class="alert alert-info">
        <span>Para adicionar fotos ao projeto, salve-o e utilize a opção <em>editar</em></span>
    </div>
    <?php endif; ?>
    </div>
</div>