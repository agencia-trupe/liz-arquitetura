<div class="conteudo-projetos projetos-paisagismo">
    <div class="interna">
        <div class="projetos-lista-slider-wrapper">
            <div class="<?php echo ( count( $projetos ) > 1 ) ? 'projetos-lista-slider' : ''; ?>">
            <?php foreach ($projetos as $key => $grupo): ?>
                <div role="projeto-slide">
                    <div class="projetos-lista-thumbs-wrapper">
                    <?php foreach ($grupo as $projeto): ?>
                        <a href="<?php echo site_url('projetos/' . $projeto_tipo . '/' . $projeto->id) ?>" class="projeto-thumb">
                            <span class="projeto-thumb-img">
                                <img src="<?php echo base_url('assets/img/projetos/capas/' . $projeto->capa) ?>" alt="">
                            </span>
                            <span class="projeto-thumb-hover">
                                <span class="projeto-titulo"><?php echo $projeto->titulo ?></span>
                            </span>
                        </a>
                    <?php endforeach ?>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
    </div> 
</div>
<div class="clearfix"></div>