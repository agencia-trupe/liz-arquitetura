<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Categoria de Projetos</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'projetos/admin_tipos/processa';
                    break;
                
                default:
                    $action = 'projetos/admin_tipos/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$tipo->id; ?>" class="id" />
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'nome',
        'value' => set_value('nome', ( $acao == 'editar' ) ? $tipo->nome : '')
    )); ?>
    <?=form_error('nome'); ?>
    
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('projetos/admin_tipos/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    </div>
</div>