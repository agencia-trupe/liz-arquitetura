<div class="row-fluid">
    <div class="span9">
            <legend>Categorias de Projeto 
                <?php echo anchor('painel/projetos/cadastrar', 'Novo projeto', 'class="btn btn-info btn-mini"'); ?> 
                <?php echo anchor('painel/tipos/cadastrar', 'Nova categoria', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-tipos btn btn-mini btn-info">ordenar categorias</a>
                <a href="#" class="salvar-ordem-tipos hide btn btn-mini btn-warning">salvar ordem</a>
            </legend>
            <div class="alert alert-info hide tipos-mensagem">
                <span>Para ordenar, clique no nome da categoria e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>

     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="span6">Título</th><th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipos as $tipo): ?>
                <tr id="tipo_<?php echo $tipo->id ?>">
                    <td><?=$tipo->nome; ?></td>
                    <td><?=anchor('projetos/admin_projetos/lista/' . $tipo->slug, 'Listar Projetos', 'class="btn btn-mini btn-info"'); ?>
                        <?=anchor('projetos/admin_tipos/editar/' . $tipo->id, 'Editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('projetos/admin_tipos/deleta_tipo/' . $tipo->id, 'Remover', 'class="btn btn-mini btn-danger"'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>