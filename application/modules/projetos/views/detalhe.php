<div class="conteudo-projetos projetos-paisagismo projetos-detalhe">
    <div class="interna">
        <h1><?php echo $projeto->titulo ?></h1>
        <div class="projetos-detalhe-wrapper">
            <div id="projetos-detalhe-tabs">
                <div class="projetos-detalhe-ampliada">
                    <div id="foto1">
                        <img src="<?php echo base_url('assets/img/projetos/' . $projeto->capa ) ?>" alt="<?php echo $projeto->titulo ?>">
                    </div>
                    <?php if($fotos): ?>
                        <?php foreach ($fotos as $foto): ?>
                            <div id="foto<?php echo $foto->id ?>">
                                <img src="<?php echo base_url('assets/img/projetos/' . $foto->imagem ) ?>" alt="<?php echo $projeto->titulo ?>">
                                <div class="clearfix"></div>
                            </div>
                        <?php endforeach ?>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <div class="controles">
                    <a href="#" id="up"></a>
                    <a href="#" id="down"></a> 
                </div> 
                <div class="projetos-detalhe-miniaturas-container">
                    
                    <div class="projetos-detalhe-miniaturas-inner">
                        <ul class="projetos-detalhe-miniaturas">
                            <li>
                                <a class="bwWrapper" href="#foto1">
                                    <img src="<?php echo base_url('assets/img/projetos/thumbs/' . $projeto->capa) ?>" alt="<?php echo $projeto->titulo ?>">  
                                </a>
                            </li>
                            <?php if($fotos): ?>
                                <?php foreach ($fotos as $foto): ?>
                                    <li>
                                        <a class="bwWrapper" href="#foto<?php echo $foto->id ?>">
                                            <img src="<?php echo base_url('assets/img/projetos/thumbs/' . $foto->imagem) ?>" alt="<?php echo $projeto->titulo ?>">  
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            <?php endif; ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="controle-social">
                    <a href="<?php echo site_url('projetos/interiores') ?>" class="projetos-detalhe-voltar">
                        voltar
                    </a>
                    <div class="projetos-detalhe-social">
                        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt">Tweetar</a>
                        <div class="g-plusone" data-size="medium"></div>
                        <script src="//platform.linkedin.com/in.js" type="text/javascript">
                         lang: en_US
                        </script>
                        <script type="IN/Share" data-counter="right"></script>
                        <iframe class="facebook-share" src="//www.facebook.com/plugins/like.php?href=<?php echo current_url(); ?>&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=true&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=355115464591773" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:85px; height:21px;" allowTransparency="true"></iframe>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="separador-pagina"></div>
    <div class="interna">
        <div class="outros-projetos">
            <h2>Veja outros projetos</h1>
            <div class="outros-projetos-wrapper">
                <?php foreach ($outros as $key => $outro): ?>
                    <a href="<?php echo site_url( 'projetos/' . $outro['tipo'] . '/' . $outro['id'] ) ?>" class="outro-projeto">
                        <img width="130" height="130" src="<?php echo base_url('assets/img/projetos/capas/' . $outro['capa']) ?>" alt="<?php echo $outro['titulo'] ?>">
                        <span class="hover"></span>
                    </a>
                    <div class="outro-projeto">
                        
                    </div>                    
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>