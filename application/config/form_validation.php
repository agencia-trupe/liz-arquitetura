<?php 
$config = array(
    'tipos' => array(
             array(
                     'field' => 'nome',
                     'label' => 'título',
                     'rules' => 'required'
                  ),                     
        ),
     'paginas' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),                       
        ),
     'projetos' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),               
        ),
     'mostras' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),                  
        ),
     'colaborador' => array(
             array(
                     'field' => 'nome',
                     'label' => 'nome',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'texto',
                     'label' => 'texto',
                     'rules' => 'required'
                  ),                  
        ),
    'servicos' => array(
             array(
                     'field' => 'titulo',
                     'label' => 'título',
                     'rules' => 'required'
                  ),
             array(
                     'field' => 'descricao',
                     'label' => 'descricao',
                     'rules' => 'required'
                  ),                       
        ),
    'dicas' => array(
            array(
                     'field' => 'titulo_nav',
                     'label' => 'título (navegação)',
                     'rules' => 'required'
                  ),
            array(
                     'field' => 'titulo_conteudo',
                     'label' => 'título (conteúdo)',
                     'rules' => 'required'
                  ),
            array(
                'field' => 'texto',
                'label' => 'texto',
                'rules' => 'required'
                ),
        ),
    'endereco' => array(
            array(
                'field' => 'endereco',
                'label' => 'endereço',
                'rules' => 'required' 
                ),
            array(
                'field' => 'bairro',
                'label' => 'bairro',
                'rules' => 'required' 
                ),
            array(
                'field' => 'cidade',
                'label' => 'cidade',
                'rules' => 'required' 
                ),
            array(
                'field' => 'uf',
                'label' => 'uf',
                'rules' => 'required|max_length[2]' 
                ),
            array(
                'field' => 'cep',
                'label' => 'cep',
                'rules' => 'required' 
                ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required' 
                ),
            array(
                'field' => 'telefone',
                'label' => 'telefone',
                'rules' => 'required' 
                ),
        ),
        'midia' => array(
            array(
                'field' => 'titulo',
                'label' => 'título',
                'rules' => 'required'  
            ),
        ),
        'contato' => array(
            array(
                'field' => 'nome',
                'label' => 'nome',
                'rules' => 'required'
            ),
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'mensagem',
                'label' => 'mensagem',
                'rules' => 'required'
            ),
        )
     );