<div class="span6 offset3">
    <div class="well">
        <div class="row-fluid">
                <h2 style="color:#0086b3;">Consistere HRM</h2>
                <h4 style="color:#0086b3;">Criar conta</h4>
                <hr>

                <h3>Dados básicos</h3>

                <?php 
                echo form_open('login/create_member');
                echo form_input('first_name', set_value('first_name', 'Nome'), 'class="span11"');
                echo form_input('last_name', set_value('last_name', 'Sobrenome'), 'class="span11"');
                echo form_input('email_adress', set_value('email_adress', 'Email'), 'class="span11"');
                ?>
                <h3>Dados de Login</h3>
                <?php 
                echo form_input('username', set_value('username', 'Login'), 'class="span11"');
                ?>
                <label for="password">Senha</label>
                <?php
                echo form_password('password', set_value('password'), 'class="span11"');
                ?>
                <label for="password2">Repita a senha</label>
                <?php
                echo form_password('password2', set_value('password2'), 'class="span11"');
                ?>
                <div class="row-fluid">
                <?php
                echo form_submit('submit', 'Criar conta', 'class="btn btn-small btn-info span3"');
                echo anchor('login', 'Cancelar', 'class="btn btn-small btn-warning span3"')
                ?>
                </div>

                <?php echo validation_errors('<p class="error">'); ?>
        </div>
    </div>
</div>
    