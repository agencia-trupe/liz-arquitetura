<div class="span12">
<div class="span4">
    <div class="well">
        <div class="row">
        <div id="login_form">
            <h2 style="color:#0086b3;">Consistere HRM</h2>
            <h4 style="color:#0086b3;">Login</h4>
            <hr>
            <?php
            echo form_open('login/validate_credentials');
            echo form_input('username', 'Username', 'class="span11"');
            echo form_password('password', 'Password', 'class="span11"');
            ?>
            <div class="row">
            <?php
            echo form_submit('submit', 'Entrar', 'class="btn btn-small btn-info span4"');
            echo anchor('login/signup', 'Criar conta', 'class="btn btn-small span4"');
            ?>
            </div>
        </div>
        </div>
    </div>
</div>
</div>