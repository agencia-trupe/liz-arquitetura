$(function() {
  $('.slides').bxSlider({
    controls:false,
    auto: true
  });
  $('a.projeto-thumb:odd').addClass('projeto-thumb-bottom');

});


$(document).ready(function() {
    var animationOffset = $('.projetos-detalhe-miniaturas-container').height() - $('.projetos-detalhe-miniaturas-inner').height();
    
    if ( $('.projetos-detalhe-miniaturas-inner').height() > $('.projetos-detalhe-miniaturas-container').height() ) {
        
        $("#down").toggle();
        var mTop = 0;
        
        $('#down').click(function () {
            var mTop =  parseInt($('.projetos-detalhe-miniaturas-inner').css('margin-top') );
            console.log(mTop);
            console.log(animationOffset);
            if( mTop >= animationOffset + 85 ){
              $('.projetos-detalhe-miniaturas-inner').animate({ "marginTop": "-=98px" }, 800, function() { $('.projetos-detalhe-miniaturas-inner').stop(); });
              $('#up').show();
            }
            else {
              $('#down').hide();
            }
            return false;
        });

        $('#up').click(function () {
            var mTop =  parseInt($('.projetos-detalhe-miniaturas-inner').css('margin-top') );
            console.log(mTop);
            console.log(animationOffset);
            if( mTop <= -98 ){
              $('.projetos-detalhe-miniaturas-inner').animate({ "marginTop": "+=98px" }, 800, function() { $('.projetos-detalhe-miniaturas-inner').stop(); });
              $('#down').show();
            }
            else {
              $('#up').hide();
            }
            return false;
        });        
    }
});



$(window).load(function(){
    $(".bwWrapper").BlackAndWhite({
        hoverEffect : false, // default true
        // set the path to BnWWorker.js for a superfast implementation
        webworkerPath : false,
        // for the images with a fluid width and height 
        responsive:true,
        // to invert the hover effect
        invertHoverEffect: false,
        speed: { //this property could also be just speed: value for both fadeIn and fadeOut
            fadeIn: 200, // 200ms for fadeIn animations
            fadeOut: 800 // 800ms for fadeOut animations
        },
        onImageReady:function(img) {
          $('.BWFilter').css('display','none');
        }
    });
});

$(function() {
  $('.projetos-lista-slider').bxSlider();
  $('.clipping-slider').bxSlider({
    pager:false
  });
  $('#projetos-detalhe-tabs').tabs();

});
//Botão Twitter
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

//Botão +1
window.___gcfg = {lang: 'pt-BR'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
  
//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});

//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
 $('#contato-form').submit(function() {
    $('#message').html('Enviando...'); 
    var form_data = {
      nome : $('.nome_form').val(),
      email : $('.email_form').val(),
      mensagem : $('.mensagem_form').val(),
       ajax : '1'
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/contato/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        alert(msg);        
      }
    });
    return false;
  });
}); 

